package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import dao.BuyDAO;

/**
 * 購入履歴画面
 * @author d-yamaguchi
 *
 */
@WebServlet("/UserBuyHistoryDetail")
public class UserBuyHistoryDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		try {

			int id = Integer.parseInt(request.getParameter("id"));
			request.setAttribute("id", id);

//			-----------------------------------------------------------------------------
			//	詳細１　　-----------------------------------------
			BuyDAO buyDao = new BuyDAO();
			BuyDataBeans buyData = buyDao.getBuyData(id);

			request.setAttribute("buyData", buyData);

//			-----------------------------------------------------------------------------
			//	詳細２　　-----------------------------------------
			BuyDAO buyDao2 = new BuyDAO();
			ArrayList<BuyDataBeans> buyData2 = buyDao2.getBuyData2(id);

			request.setAttribute("buyData2", buyData2);

//			-----------------------------------------------------------------------------

			request.getRequestDispatcher(EcHelper.USER_BUY_HISTORY_DETAIL_PAGE).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}





}

