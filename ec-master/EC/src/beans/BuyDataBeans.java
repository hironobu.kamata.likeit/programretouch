package beans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * 購入データ
 * @author d-yamaguchi
 *
 */
public class BuyDataBeans  implements Serializable {
	private int id;
	private int userId;
	private int totalPrice;
	private int delivertMethodId;
	private Date buyDate;

	private String deliveryMethodName;
	private int deliveryMethodPrice;

	private String name;
	private int price;


	public BuyDataBeans(int id, int userId, int totalPrice, int delivertMethodId, String  deliveryMethodName, Date buyDate) {
		this.id = id;
		this.userId = userId;
		this.totalPrice = totalPrice;
		this.delivertMethodId = delivertMethodId;
		this.deliveryMethodName = deliveryMethodName;
		this.buyDate = buyDate;
	}

	public BuyDataBeans(String name, int price) {
		this.name = name;
		this.price = price;
	}

//	-----------------------------------------------
	public BuyDataBeans() {}

//	-----------------------------------------------
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
//	-----------------------------------------------
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}

//	-----------------------------------------------
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
//	-----------------------------------------------
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
//	-----------------------------------------------
	public int getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}

//	-----------------------------------------------
	public int getDelivertMethodId() {
		return delivertMethodId;
	}
	public void setDelivertMethodId(int delivertMethodId) {
		this.delivertMethodId = delivertMethodId;
	}
//	-----------------------------------------------
	public Date getBuyDate() {
		return buyDate;
	}
	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}
//	-----------------------------------------------
	public String getDeliveryMethodName() {
		return deliveryMethodName;
	}
	public void setDeliveryMethodName(String deliveryMethodName) {
		this.deliveryMethodName = deliveryMethodName;
	}
//	-----------------------------------------------
	public String getFormatDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH時mm分");
		return sdf.format(buyDate);
	}
//	-----------------------------------------------
	public String getFormatTotalPrice() {
		return String.format("%,d", this.totalPrice);
	}
	public int getDeliveryMethodPrice() {
		return deliveryMethodPrice;
	}

//	-----------------------------------------------
	public void setDeliveryMethodPrice(int deliveryMethodPrice) {
		this.deliveryMethodPrice = deliveryMethodPrice;
	}




}
